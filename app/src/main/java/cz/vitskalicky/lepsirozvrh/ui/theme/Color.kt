package cz.vitskalicky.lepsirozvrh.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val BlueGrey700 = Color(0xFF455a64)
val Yellow800 = Color(0xfff9a825)
val Yellow900 = Color(0xfff57f17)